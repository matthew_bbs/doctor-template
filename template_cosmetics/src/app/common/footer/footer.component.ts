import {
  Component,
  OnInit,
  ChangeDetectorRef,
  AfterViewInit,
} from '@angular/core';
import { Router, Event, NavigationStart } from '@angular/router';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css'],
})
export class FooterComponent implements OnInit, AfterViewInit {
  splitVal;
  url;
  base;
  page;
  showSite = false;
  constructor(public router: Router, private cdr: ChangeDetectorRef) {}

  ngOnInit(): void {
    this.router.events.subscribe((event: Event) => {
      if (event instanceof NavigationStart) {
        this.splitVal = event.url.split('/');
        this.base = this.splitVal[1];
        this.page = this.splitVal[2];
      }
    });
    if (this.base === 'doctor' && this.page === 'dashboard') {
      this.showSite = true;
    } else {
      this.showSite = false;
    }
    if (this.base === 'doctor' && this.page === 'patients') {
      this.showSite = false;
    } else {
      this.showSite = true;
    }
  }
  ngAfterViewInit() {
    this.cdr.detectChanges();
  }
}
